import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> myInts = Arrays.asList();
        int sum = myInts.stream()
                .mapToInt(i -> i)
                .sum();

        myInts.stream()
                .mapToInt(i -> i)
                .average()
                .ifPresent(avg -> System.out.println("Srednia " + avg));

        IntSummaryStatistics intSummaryStatistics = myInts.stream()
                .mapToInt(i -> i)
                .summaryStatistics();

        System.out.println("Suma");
        System.out.println(sum);

    }
}
